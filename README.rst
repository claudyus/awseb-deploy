AWS EB DEPLOY
================

Example of AWS Elastic Beanstalk deploy using Docker


From zero to stars
--------------------

1. Create a EB poject
2. Install the `awsebcli` package and init local env using `eb init`
3. (Optionally) set and verify ENV vars usinf `eb printenv` and `eb setenv`
